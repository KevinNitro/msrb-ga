COUNT_ACCOUNTS=$(cat accounts.json | jq '. | length')
PARALLEL_TASK_PER_BOT=${{ env.PARALLEL_TASK_PER_BOT }}
ACCOUNTS_PER_TASK=${{ env.ACCOUNTS_PER_TASK }}

PROCESSING_DIRECTORY=1
j=1
p=1
o=1
ADDED_ACCOUNTS=1
stop=0

mkdir MSRB

cat accounts.json | jq -c '.[]' | while read i
do
    if [ ! -d "MSRB/$PROCESSING_DIRECTORY" ]
    then
        mkdir MSRB/$PROCESSING_DIRECTORY
    fi 
    if (( p <= $ACCOUNTS_PER_TASK ))
    then
        if [ ! -e "MSRB/$PROCESSING_DIRECTORY/accounts_$o.json" ]
        then
            touch "MSRB/$PROCESSING_DIRECTORY/accounts_$o.json"
            echo "[" > "MSRB/$PROCESSING_DIRECTORY/accounts_$o.json"
        fi
        echo -e "$i\c" >> "MSRB/$PROCESSING_DIRECTORY/accounts_$o.json"
        (( ADDED_ACCOUNTS++ ))
        if  (( $ADDED_ACCOUNTS > $COUNT_ACCOUNTS ))
        then
            echo "]" >> "MSRB/$PROCESSING_DIRECTORY/accounts_$o.json"
            stop=1
            break
        fi
        (( p++ ))
    fi
    if (( $ADDED_ACCOUNTS <= $COUNT_ACCOUNTS )) && [ $stop -ne 1 ]
    then
        if (( p > $ACCOUNTS_PER_TASK ))
        then
            echo "]" >> "MSRB/$PROCESSING_DIRECTORY/accounts_$o.json"
            p=1
            (( o++ ))
        else
            echo "," >> "MSRB/$PROCESSING_DIRECTORY/accounts_$o.json"
        fi
        if (( o > $PARALLEL_TASK_PER_BOT ))
        then
            o=1
            (( PROCESSING_DIRECTORY++ ))
        fi
    fi
done