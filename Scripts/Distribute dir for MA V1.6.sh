COUNT_ACCOUNTS=$(cat accounts.json | jq '. | length')
PARALLEL_TASK_PER_JOB=2
ACCOUNTS_PER_TASK=2
PROCESSING_DIRECTORY=1
accounts_per_task=1
parallel_task_per_job=1
added_account=0
mkdir MSRB
mkdir MSRB/1

cat accounts.json | jq -c '.[]' | while read i
do
    if [[ $accounts_per_task -gt $ACCOUNTS_PER_TASK ]]; then
        sed -i '$ s/,$//' "MSRB/$PROCESSING_DIRECTORY/accounts_$parallel_task_per_job.json"
        echo "]" >> "MSRB/$PROCESSING_DIRECTORY/accounts_$parallel_task_per_job.json"
        jq -r '.[].username' "MSRB/$PROCESSING_DIRECTORY/accounts_$parallel_task_per_job.json" | sort -V >> "MSRB/$PROCESSING_DIRECTORY/accounts_for_session_cache.txt"
        accounts_per_task=1
        ((parallel_task_per_job+=1))
        if [[ $parallel_task_per_job -gt $PARALLEL_TASK_PER_JOB ]]; then
            parallel_task_per_job=1
            ((PROCESSING_DIRECTORY+=1))
            mkdir MSRB/$PROCESSING_DIRECTORY
        fi
    fi
    if [ ! -e "MSRB/$PROCESSING_DIRECTORY/accounts_$parallel_task_per_job.json" ];then
        touch "MSRB/$PROCESSING_DIRECTORY/accounts_$parallel_task_per_job.json"
        echo "[" > "MSRB/$PROCESSING_DIRECTORY/accounts_$parallel_task_per_job.json"
    fi
    echo -e "$i," >> "MSRB/$PROCESSING_DIRECTORY/accounts_$parallel_task_per_job.json"
    (( added_account+=1 ))
    (( accounts_per_task+=1 ))
    if [[ $added_account -eq $COUNT_ACCOUNTS ]]; then
        sed -i '$ s/,$//' "MSRB/$PROCESSING_DIRECTORY/accounts_$parallel_task_per_job.json"
        echo "]" >> "MSRB/$PROCESSING_DIRECTORY/accounts_$parallel_task_per_job.json"
        jq -r '.[].username' "MSRB/$PROCESSING_DIRECTORY/accounts_$parallel_task_per_job.json" | sort -V >> "MSRB/$PROCESSING_DIRECTORY/accounts_for_session_cache.txt"
    fi
done