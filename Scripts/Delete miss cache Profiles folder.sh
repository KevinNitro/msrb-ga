Profiles_path="MSRB/${{ env.Current-Bot }}/Profiles"
accounts_for_session_cache_path="MSRB/${{ env.Current-Bot }}/accounts_for_session_cache.txt"

mapfile -t usernames < "$accounts_for_session_cache_path"

for folder in "$Profiles_path"/*; do
  folder_name=$(basename "$folder")
  
  if [[ ! " ${usernames[@]} " =~ " $folder_name " ]]; then
    echo "Deleting folder: $folder"
    rm -rf "$folder"
  fi
done