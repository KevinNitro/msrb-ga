COUNT_ACCOUNTS=$(cat accounts.json | jq '. | length')
number_of_accounts_per_job=$(($COUNT_ACCOUNTS/20))
mod_number_of_accounts_per_job=$(($COUNT_ACCOUNTS%20))
checking_value=1
PROCESSING_DIRECTORY=1
trigger_mod_distribute=false
added_account=0
mkdir MSRB

if [[ $number_of_accounts_per_job -ne 0 ]]; then
    if [[ $mod_number_of_accounts_per_job -ne 0 ]]; then
        ((number_of_accounts_per_job+=1))
        trigger_mod_distribute=true
    fi
else
    number_of_accounts_per_job=1
fi

cat accounts.json | jq -c '.[]' | while read i
do
    if [[ $PROCESSING_DIRECTORY -gt $mod_number_of_accounts_per_job && $trigger_mod_distribute = "true" ]]; then
        ((number_of_accounts_per_job-=1))
        trigger_mod_distribute=false
    fi
    if [[ $checking_value -gt $number_of_accounts_per_job || $added_account -eq $COUNT_ACCOUNTS ]]; then
        sed -i '$ s/,$//' "MSRB/$PROCESSING_DIRECTORY/accounts.json"
        echo "]" >> "MSRB/$PROCESSING_DIRECTORY/accounts.json"
        jq -r '.[].username' "MSRB/$PROCESSING_DIRECTORY/accounts.json" | sort -V > "MSRB/$PROCESSING_DIRECTORY/accounts_for_session_cache.txt"
        ((PROCESSING_DIRECTORY+=1))
        checking_value=1
    fi
    if [ ! -d "MSRB/$PROCESSING_DIRECTORY" ]
    then
        mkdir MSRB/$PROCESSING_DIRECTORY
        touch "MSRB/$PROCESSING_DIRECTORY/accounts.json"
        echo "[" > "MSRB/$PROCESSING_DIRECTORY/accounts.json"
    fi
    echo "$i," >> "MSRB/$PROCESSING_DIRECTORY/accounts.json"
    ((added_account+=1))
    ((checking_value+=1))
    if [[ $added_account -eq $COUNT_ACCOUNTS ]]; then
        sed -i '$ s/,$//' "MSRB/$PROCESSING_DIRECTORY/accounts.json"
        echo "]" >> "MSRB/$PROCESSING_DIRECTORY/accounts.json"
        jq -r '.[].username' "MSRB/$PROCESSING_DIRECTORY/accounts.json" | sort -V > "MSRB/$PROCESSING_DIRECTORY/accounts_for_session_cache.txt"
    fi
done