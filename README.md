# 𝕄𝕀ℂℝ𝕆𝕊𝕆𝔽𝕋 ℝ𝔼𝕎𝔸ℝ𝔻𝕊 𝔹𝕆𝕋 ℝ𝕌ℕℕ𝕀ℕ𝔾 𝕆ℕ 𝔾𝕀𝕋ℍ𝕌𝔹 𝔸ℂ𝕋𝕀𝕆ℕ

This project may not be maintained anymore because 𝕄𝕊ℝ changed their way to earn and redeem. It make me have not much reasons for maintain it, unless I wan't to learn code and do something with the scripts

---

- Latest version: `V1.6`
- [Version History][Changelog]
- Please use it on your own responsibility, don't report _"This script is bla bla bla, redeem got banned, my account got banned/suspend..."_ Farming 𝕄𝕚𝕔𝕣𝕠𝕤𝕠𝕗𝕥 ℝ𝕖𝕨𝕒𝕣𝕕𝕤 using 𝔹𝕠𝕥 violates their ToS, you even use their VPS to farm it, then don't be childish

## INFORMATION ✏️

- Source of **CLI BOT**: [𝕗𝕒𝕣𝕤𝕙𝕒𝕕𝕫𝟙𝟡𝟡𝟟's bot repo][𝕗𝕒𝕣𝕤𝕙𝕒𝕕𝕫𝟙𝟡𝟡𝟟's bot repo]
- You can contact me via [Telegram][Telegram]
  > Don't PM me and ask about the problem / question of bot aspect. Ask them in [𝕗𝕒𝕣𝕤𝕙𝕒𝕕𝕫𝟙𝟡𝟡𝟟's Telegram group][𝕗𝕒𝕣𝕤𝕙𝕒𝕕𝕫𝟙𝟡𝟡𝟟's Telegram group]
- 𝔾𝕚𝕥𝕙𝕦𝕓 𝔸𝕔𝕥𝕚𝕠𝕟 runners are always in USA _(unchangeable)_
- This bot will run on 𝔾𝕚𝕥𝕙𝕦𝕓 𝔸𝕔𝕥𝕚𝕠𝕟 on schedule with **Cron Schedule**
- Prefer using private repo _(To keep the repo at low risk)_
- If you are already using older 𝕄𝕊ℝ𝔹 𝔾𝔸 version and face no issue, then not need to update the script everytime there is newer version
  > **Warning**: Runing on 𝔾𝕚𝕥𝕙𝕦𝕓 𝔸𝕔𝕥𝕚𝕠𝕟 may violent Github ToS. Create on your own responsibility

---

## DIFFERENT SCRIPTS OF MSRB 📄

### [MSRB Original:][MSRB Original]

- This will run the bot sequently each account in 1 job. So it can only farm about 3 𝕄𝕚𝕔𝕣𝕠𝕤𝕠𝕗𝕥 ℝ𝕖𝕨𝕒𝕣𝕕 𝔸𝕔𝕔𝕠𝕦𝕟𝕥𝕤 on 1 𝔾𝕚𝕥𝕙𝕦𝕓 𝔸𝕔𝕔𝕠𝕦𝕟𝕥 in private repo
- Support all features of **𝕄𝕊ℝ𝔹 𝔾𝔸**
- Safest script for free 𝔾𝕚𝕥𝕙𝕦𝕓 𝔸𝕔𝕔𝕠𝕦𝕟𝕥𝕤

### [MSRB Multiple:][MSRB Multiple]

- Automatically split your `accounts.json` into multiple files and distribute your accounts with appropriate number of accounts
- Each split `accounts.json` file:
  - Contains `YourNumberOfAccounts div 20` and `+1` if `(YourNumberOfAccounts mod 20) not equal to 0` _(accounts)_. The folders after `YourNumberOfAccounts mod 20`th folder will contain `YourNumberOfAccounts div 20` _(accounts)_ _(because it's designed to distribute as much as it can - max 20 jobs)_
  - run by 1 job _(20 jobs in total with different IP)_

<Details>
<summary>
Ex: 👉 Click here to expand 👈
</summary>
If you have 74 accounts in `accounts.json`, and your `accounts.json` file like this:

```
[
    {
      "username": "KevinNitro1@gmail.com",
      "password": "password123!"
    },
    {
      "username": "KevinNitro2@gmail.com",
      "password": "password123!"
    },
    {
      "username": "KevinNitro3@gmail.com",
      "password": "password123!"
    },
    {
      "username": "KevinNitro4@gmail.com",
      "password": "password123!"
    },
    {
      "username": "KevinNitro5@gmail.com",
      "password": "password123!"
    },
    {
      "username": "KevinNitro6@gmail.com",
      "password": "password123!"
    },
    ...
]
```

It will split like this

```
MSRB
├── 1
│   └── accounts.json
├── 2
│   └── accounts.json
├── 3
│   └── accounts.json
├── 4
│   └── accounts.json
├── 5
│   └── accounts.json
├── 6
│   └── accounts.json
├── 7
│   └── accounts.json
├── 8
│   └── accounts.json
├── 9
│   └── accounts.json
├── 10
│   └── accounts.json
├── 11
│   └── accounts.json
├── 12
│   └── accounts.json
├── 13
│   └── accounts.json
├── 14
│   └── accounts.json
├── 15
│   └── accounts.json
├── 16
│   └── accounts.json
├── 17
│   └── accounts.json
├── 18
│   └── accounts.json
├── 19
│   └── accounts.json
└── 20
    └── accounts.json
```

And the first folder will contain first 4 accounts, second folder will contain next 4 accounts,... until the 5th folder, it will only contain 3 accounts and so on. This because this script tries to devide your accounts in all 20 folder as well as it can

</Details>

### [MSRB Multiple Advanced:][MSRB Multiple Advanced]

- It will auto split your `accounts.json` into multiple files from your demand _(it will follow `ACCOUNTS_PER_TASK` & `PARALLEL_TASK_PER_JOB`)_ and run on each separate jobs
- It will distribute all of your accounts from above to below, even create more than 20 folders of bot to run. But it will run only 20 first folders of bot, the rest aren't.
  > Note: At this time! It won't work if you set `PARALLEL_TASK_PER_JOB` >= 2, because idk how to make it run parralelly tasks _(webdriver is busy)_ 🥴. If you know how to implement it, please create a pull request or DM me via Telegram

<Details>
<summary>
Ex: 👉 Click here to expand 👈
</summary>

If you have 97 accounts in `accounts.json`, `ACCOUNTS_PER_TASK=3`, `PARALLEL_TASK_PER_JOB=2`, and your `accounts.json` file like this:

```
[
    {
      "username": "KevinNitro1@gmail.com",
      "password": "password123!"
    },
    {
      "username": "KevinNitro2@gmail.com",
      "password": "password123!"
    },
    {
      "username": "KevinNitro3@gmail.com",
      "password": "password123!"
    },
    {
      "username": "KevinNitro4@gmail.com",
      "password": "password123!"
    },
    {
      "username": "KevinNitro5@gmail.com",
      "password": "password123!"
    },
    {
      "username": "KevinNitro6@gmail.com",
      "password": "password123!"
    },
    ...
]
```

It will split like this

```MSRB
├── 1
│   ├── accounts_1.json
│   └── accounts_2.json
├── 2
│   ├── accounts_1.json
│   └── accounts_2.json
├── 3
│   ├── accounts_1.json
│   └── accounts_2.json
├── 4
│   ├── accounts_1.json
│   └── accounts_2.json
├── 5
│   ├── accounts_1.json
│   └── accounts_2.json
├── 6
│   ├── accounts_1.json
│   └── accounts_2.json
├── 7
│   ├── accounts_1.json
│   └── accounts_2.json
├── 8
│   ├── accounts_1.json
│   └── accounts_2.json
└── 9
│   ├── accounts_1.json
│   └── accounts_2.json
├── 10
│   ├── accounts_1.json
│   └── accounts_2.json
├── 11
│   ├── accounts_1.json
│   └── accounts_2.json
├── 12
│   ├── accounts_1.json
│   └── accounts_2.json
├── 13
│   ├── accounts_1.json
│   └── accounts_2.json
├── 14
│   ├── accounts_1.json
│   └── accounts_2.json
├── 15
│   ├── accounts_1.json
│   └── accounts_2.json
├── 16
│   ├── accounts_1.json
│   └── accounts_2.json
└── 17
    └── accounts_1.json
```

- Each split account_\*.json will contain 3 accounts. The accounts_\*.json of the last folder will contain 1 accounts_1.json and there will be 1 rest account in it.
</Details>

---

## NOTE BEFORE CREATTING 𝕄𝕊ℝ𝔹 𝔾𝔸 📝

- Use other 𝔾𝕚𝕥𝕙𝕦𝕓 𝔸𝕔𝕔𝕠𝕦𝕟𝕥𝕤, don't use your main account
- Encourage use [MSRB Original][MSRB Original], farm 3 𝕄𝕚𝕔𝕣𝕠𝕤𝕠𝕗𝕥 𝕒𝕔𝕔𝕠𝕦𝕟𝕥𝕤 on 1 Github account with 1 private repo
- Support `--session` using **𝔾𝕚𝕥𝕙𝕦𝕓 𝔸𝕔𝕥𝕚𝕠𝕟 ℂ𝕒𝕔𝕙𝕖**
  > [Check here](https://gitlab.com/KevinNitro/msrb-ga/-/edit/main/README.md#iv-how-does-session-cache-work) to understand how session cache work
- With 𝔾𝕚𝕥𝕙𝕦𝕓 𝔸𝕔𝕥𝕚𝕠𝕟 quota _(2000 mins/month)_, you can farm about 3 accounts once a day _(using `--virtual-display` takes more time than `--headless`)_
  > [Check here][Github Action limits] to see more information about 𝔾𝕚𝕥𝕙𝕦𝕓 𝔸𝕔𝕥𝕚𝕠𝕟 𝕝𝕚𝕞𝕚𝕥𝕤, 𝕣𝕖𝕤𝕠𝕦𝕣𝕔𝕖𝕤,...
- For advanced using, read and modify the **.yml** file
- If you are new to 𝔾𝕚𝕥𝕙𝕦𝕓 and don't know how to make a **𝔾𝕚𝕥𝕙𝕦𝕓 𝔸𝕔𝕥𝕚𝕠𝕟 𝕊𝕖𝕔𝕣𝕖𝕥**, check [here][Create Github Action Secret]

---

## STEPS TO MAKE THE BOT 💥

### I. CREATE GITHUB REPO

1. Create a Github Repository with the name not related to MSRB
2. Click on `Create a new file`
3. Enter `.github/workflows/web_app.yml` in directory
   > Any file name you want, don't use any name related to 𝕄𝕚𝕔𝕣𝕠𝕤𝕠𝕗𝕥 ℝ𝕖𝕨𝕒𝕣𝕕 𝔹𝕠𝕥, like `deploy`, `index`, `build`, `hehe`, `hoho`,...
4. Copy the script and **commit**

---

### II. FILL IN REQUIREMENTS

---

#### A. CREATE `ACCOUNTS.JSON`: YOU HAVE 2 OPTIONS:

##### 1️⃣: Create directly on repo:

1. Click on **Add file**
2. **Create new file**
3. Fill in content of `accounts.json`, name the file and commit
   > **Note**: It will store `accounts.json` file in the repo

##### 2️⃣: Fill in using Github Repo Secrets

- **Name:** `ACCOUNTS`<br>
  **Secret:** Link to the raw content of the **accounts.json**

> If you don't know how to create the `accounts.json` content raw link, check how the example of creating secret gist [here][Create secret Gist]

> **Note**: It will rewrite **accounts.json** if you use secret **ACCOUNTS**

---

#### B. EDIT CRON SHCEDULE

1. Open the your `yml` file
2. At the line **10** & **11** remove the hashtag _(#)_ at the beginning of each line to enable Cron Schedule
   > Otherwise it will not automatically run the Bot
3. Edit this line if you want to change the time of bot to run
   <br> `- cron: '0 0 * * *' # Default: run at 00:00 everyday`
4. Use those website to convert the time you want
   > [UTC converter](https://savvytime.com/converter/utc)<br> [Cron Tab Explain](https://crontab.guru/examples.html)

---

#### C. TURN FEATURES ON/OFF

> Acctually you will edit the `yml` file

1. Open your `yml` file
2. Edit the `env:` _(from line 28 to the beginning of `jobs:`)_

| **NAME OF VARIABLE**                         | **IN YML**                                                                  | **DESCRIPTION**                                                                                                                                                                                                                                         | **HOW TO USE**                                                                                                                        | **DEFAULT VALUE**                                                                                                                                   |
| -------------------------------------------- | --------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------- |
| `COMMAND`                                    | All                                                                         | Run the bot with this command<br>Don't add `--on-finish` because it will automatically add `--on-finish exit` into the `COMMAND`                                                                                                                        | Edit directly or add in **Secrets**                                                                                                   | From `DEFAULT_COMMAND`                                                                                                                              |
| `RETRY_COMMAND`                              | All                                                                         | If the Run the Bot step is failed, then this will retry the bot with this command<br>Don't add `--on-finish` because it will automatically add `--on-finish exit` into the `RETRY_COMMAND`                                                              | Edit directly or add in **Secrets**                                                                                                   | From `DEFAULT_COMMAND`                                                                                                                              |
| `DEFAULT_COMMAND`                            | All                                                                         | If you don't set `COMMAND` & `RETRY_COMMAND` values, then they will be this command                                                                                                                                                                     | Keep it default or change it into your command directly. Then `COMMAND` & `RETRY_COMMAND` will inherit this value                     | `python ms_rewards_farmer.py --virtual-display --dont-check-for-updates --repeat-shopping --superfast --skip-unusual --error --no-images --session` |
| `SPECIFIC_VERSION`                           | All                                                                         | Use the bot will specific version                                                                                                                                                                                                                       | Edit directly or add in **Secrets**<br>Take the hash of commit _(Either short or full format)_ from [here][𝕗𝕒𝕣𝕤𝕙𝕒𝕕𝕫𝟙𝟡𝟡𝟟's bot commit] | Latest version                                                                                                                                      |
| `ACCOUNTS`                                   | All                                                                         | This will be your `account.json` raw link file                                                                                                                                                                                                          | As I said, you can upload directly into the repo or create a **Secrets** with the raw link `accounts.json`                            | _None_                                                                                                                                              |
| `RETRY_THE_BOT`                              | All                                                                         | Turn `on`/`off` the steps **Retry the Bot**                                                                                                                                                                                                             | `True`/`False`                                                                                                                        | `True`                                                                                                                                              |
| `DELETE_OLD_WORKFLOWS`                       | All                                                                         | Delete old workflows which are older than `DELETE_OLD_WORKFLOWS_SECOND`                                                                                                                                                                                 | Edit directly                                                                                                                         | `True`                                                                                                                                              |
| `DELETE_OLD_WORKFLOWS_SECOND`                | All                                                                         | The second of how old are the workflows runs to be deleted                                                                                                                                                                                              | Edit directly _(second)_                                                                                                              | `1`                                                                                                                                                 |
| `CHECK_IP`                                   | All                                                                         | Check the Public IP _(IPv4)_ of the running VM of the job                                                                                                                                                                                               | `True`/`False`                                                                                                                        | `False`                                                                                                                                             |
| `PRINT_OUT_THE_LOG_AFTER_RUNNING_BOT`        | All                                                                         | Print out the `Log_accounts.json` contents into log of the workflows                                                                                                                                                                                    | `True`/`False`                                                                                                                        | `True`                                                                                                                                              |
| `COMMIT_LOG_ACCOUNTS_JSON_AFTER_RUNNING_BOT` | [`Original`][MSRB Original]                                                 | Commit the `Log_accounts.json` after running the bot into your Repo                                                                                                                                                                                     | `True`/`False`                                                                                                                        | `False`                                                                                                                                             |
| `DELETE_ARTIFACT`                            | [`Multiple`][MSRB Multiple] & [`Multiple Advanced`][MSRB Multiple Advanced] | `MSRB_Artifact` is used for distribute split account\_\*.json for each later jobs. If you don't delete it, this artifact (contains your accounts information) will be on your repo and downloadable. Set `True` to delete right away after the bot done | `True`/`False`                                                                                                                        | `True`                                                                                                                                              |
| `ACCOUNTS_PER_TASK`                          | [`Multiple Advanced`][MSRB Multiple Advanced]                               | How many accounts there are in 1 split `accounts.json`                                                                                                                                                                                                  | Edit directly                                                                                                                         | `2`                                                                                                                                                 |
| `PARALLEL_TASK_PER_JOB`                      | [`Multiple Advanced`][MSRB Multiple Advanced]                               | How many Advanced task _(run at the same time)_                                                                                                                                                                                                         | Edit directly                                                                                                                         | `1`                                                                                                                                                 |
| `CHECK_MAX_NUMBER_OF_SPLIT_ACCOUNTS`         | [`Original`][MSRB Original] & [`Multiple`][MSRB Multiple]                   | The script will check if any split `accounts.json` is exceed 6 accounts                                                                                                                                                                                 | `True`/`False`                                                                                                                        | `True`                                                                                                                                              |

> Don't add `--on-finish ACTION` because the script will add it itself. But if you change the `DEFAULT_COMMAND` then remember to add them

> If you want to switch between types, just copy those variables in `env:`<br>But watch out there may not be some variables in other types

---

#### D. RUN

Trigger the workflow by doing either:

- Go to **Action tab**, click on your workflow name, **run workflow** _(Button on the right)_, **run workflow** on main branch
- **Star** / **restar** your repo ⭐

---

# MORE INFORMATION 👶

## I. HOW TO CREATE A 𝔾𝕀𝕋ℍ𝕌𝔹 𝔸ℂ𝕋𝕀𝕆ℕ 𝕊𝔼ℂℝ𝔼𝕋

1. In your repo, click on **Settings**
2. **Secrets and Variables**
3. **Actions**
4. **New Repository Secret**

## II. HOW TO CREATE SECRET GIST FOR `ACCOUNTS.JSON` RAW LINK

1. Go to [gist.github.com](https://gist.github.com/)
2. Fill **File name including extension**: `accounts.json` _(or any name you want)_, then paste the content into it
3. Click on **Create secret gist**
4. Click on **Raw** button
5. Change the format of the url from raw link

    **Ex:**

    > `https://gist.githubusercontent.com/KevinNitroG/abcdxyz1234/raw/`needtodeletethis`/accounts.json` ➡️ `https://gist.githubusercontent.com/KevinNitroG/abcdxyz1234/raw/accounts.json`
6. Then fill in `Secret` of `ACCOUNTS`

## III. 𝔾𝕀𝕋ℍ𝕌𝔹 𝔸ℂ𝕋𝕀𝕆ℕ 𝕀ℕ𝔽𝕆ℝ𝕄𝔸𝕋𝕀𝕆ℕ

- 𝔾𝕚𝕥𝕙𝕦𝕓 𝔸𝕔𝕥𝕚𝕠𝕟 provides a virtual environment with **2-core CPU**, **7 GB RAM** memory, and **14 GB SSD** hard disk space
- 𝔾𝕚𝕥𝕙𝕦𝕓 𝔸𝕔𝕥𝕚𝕠𝕟 ℂ𝕒𝕔𝕙𝕖: 10GB each repo
- 𝔾𝕚𝕥𝕙𝕦𝕓 𝔸𝕣𝕥𝕚𝕗𝕒𝕔𝕥: 500MB in account
- Each repository can only support **20 concurrent calls**
- Private repo will be limited **2000 minutes/month** running workflows
- Check more information from here:
  - [Usage limits, billing, and administration
    ](https://docs.github.com/en/actions/learn-github-actions/usage-limits-billing-and-administration)
  - [About billing for GitHub Actions
    ](https://docs.github.com/en/billing/managing-billing-for-github-actions/about-billing-for-github-actions)

## IV. HOW DOES SESSION CACHE WORK?

- It will use 𝔾𝕚𝕥𝕙𝕦𝕓 𝔸𝕔𝕥𝕚𝕠𝕟 ℂ𝕒𝕔𝕙𝕖 to save the session caches.
- With [MSRB Original][MSRB Original] script, there is only 1 cache will be created. But others will create more than 1 cache depend on how many bot jobs your workflow run.
- Before it save the cache, it will clean the `Cache` & `Code Cache` folders inside the Profiles folder.
- 1 𝕄𝕚𝕔𝕣𝕠𝕤𝕠𝕗𝕥 ℝ𝕖𝕨𝕒𝕣𝕕𝕤 𝔸𝕔𝕔𝕠𝕦𝕟𝕥 needs about 135MB, 𝔾𝕚𝕥𝕙𝕦𝕓 𝔸𝕔𝕥𝕚𝕠𝕟 ℂ𝕒𝕔𝕙𝕖 limit is 10GB each repo. So maybe you can use `--session` for running about 75 𝕄𝕚𝕔𝕣𝕠𝕤𝕠𝕗𝕥 ℝ𝕖𝕨𝕒𝕣𝕕𝕤 𝔸𝕔𝕔𝕠𝕦𝕟𝕥s. As long as you keep the cache under 10GB in total _(not include unused cache)_. Otherwise it will mess things up.
- If you change the order, change account... in `accounts.json`, the script restores the old cache of that current job, then it auto re-caches the **Profiles**. The old caches will not remove themselves, you can ignore it. Because if you don't use that cache for a time, it will be expired and deleted.

---

# DISCLAIMER ⚠️

This repo for testing and learning how to use 𝔾𝕚𝕥𝕙𝕦𝕓 𝔸𝕔𝕥𝕚𝕠𝕟 purpose. Farming using 𝔾𝕚𝕥𝕙𝕦𝕓 𝔸𝕔𝕥𝕚𝕠𝕟 may violate 𝕄𝕚𝕔𝕣𝕠𝕤𝕠𝕗𝕥 and 𝔾𝕚𝕥𝕙𝕦𝕓's ToS. I won't be responsible for any violence.

---

# Q&A ABOUT 𝕄𝕊ℝ𝔹 𝔾𝔸 🙋

<Details>
<summary>

###### 1. Are you associate / collab with 𝕗𝕒𝕣𝕤𝕙𝕒𝕕𝕫𝟙𝟡𝟡𝟟?

</summary>

- No, this project is not contributed by anyone other than me.
</Details>

<Details>
<summary>

###### 2. How different are public & private repo?

</summary>

- Private repo is applied the time limit _(2000 mins/month)_ while public one isn't.
- Using public repo is risky. People can find and star, folk it. Then Github will detect and may ban your repo / account.
- Also if you use `MSRB Multiple` with public repo to farm a large amount of 𝕄𝕚𝕔𝕣𝕠𝕤𝕠𝕗𝕥 ℝ𝕖𝕨𝕒𝕣𝕕𝕤 𝔸𝕔𝕔𝕠𝕦𝕟𝕥𝕤, it will be riskier due to high usage of resources, which will lead to account banning.
</Details>

<Details>
<summary>

###### 3. Can I know how the script work?

</summary>

- The script will clone the bot from [𝕗𝕒𝕣𝕤𝕙𝕒𝕕𝕫𝟙𝟡𝟡𝟟 github bot repo][𝕗𝕒𝕣𝕤𝕙𝕒𝕕𝕫𝟙𝟡𝟡𝟟's bot repo] to the running environment storage. Then it setups the bot and runs it.
- It is different from having a repo with bot code already.
</Details>

<Details>
<summary>

###### 4. If I want to farm a large amount of 𝕄𝕚𝕔𝕣𝕠𝕤𝕠𝕗𝕥 ℝ𝕖𝕨𝕒𝕣𝕕𝕤 𝔸𝕔𝕔𝕠𝕦𝕟𝕥𝕤, what should I do?

</summary>

- Please don't abuse too much on free platform.
- Use [MSRB Original][MSRB Original]. This is the safest way.
- If you have about 10 𝕄𝕚𝕔𝕣𝕠𝕤𝕠𝕗𝕥 ℝ𝕖𝕨𝕒𝕣𝕕𝕤 𝔸𝕔𝕔𝕠𝕦𝕟𝕥𝕤 and still want to use this script, then split it into multiple of `accounts.json` file _(each file contains 3 𝕄𝕚𝕔𝕣𝕠𝕤𝕠𝕗𝕥 ℝ𝕖𝕨𝕒𝕣𝕕𝕤 𝔸𝕔𝕔𝕠𝕦𝕟𝕥𝕤 max)_; create multiple 𝔾𝕚𝕥𝕙𝕦𝕓 𝔸𝕔𝕔𝕠𝕦𝕟𝕥𝕤; create 1 repo in 1 account and run 1 spit `accounts.json` file.
- If you accept the risk of banning your 𝔾𝕚𝕥𝕙𝕦𝕓 account, then use [MSRB Multiple][MSRB Multiple] or [`Multiple Advanced`][MSRB Multiple Advanced] _(there is a 𝕡𝕒𝕚𝕕 𝕘𝕚𝕥𝕙𝕦𝕓 𝕦𝕤𝕖𝕣 using it and I will update later about him)_
</Details>

<Details>
<summary>

###### 5. Do you track users usage / accounts?

</summary>

- If you are afraid of it, then don't use MSRB GA 😌
</Details>

<Details>
<summary>

###### 6. Why do you suggest use [MSRB Original][MSRB Original] but also create [MSRB Multiple][MSRB Multiple] & [`Multiple Advanced`][MSRB Multiple Advanced]?

</summary>

- The script is originally created for my personal use and learning coding purpose. There isn't limitation for me to create them 😗.
- Using [MSRB Multiple][MSRB Multiple] & [`Multiple Advanced`][MSRB Multiple Advanced] has higher risk which lead to ban/suspend your Github/𝕄𝕚𝕔𝕣𝕠𝕤𝕠𝕗𝕥 account.
- I had been using [MSRB Original][MSRB Original] V1.2 until 𝕄𝕚𝕔𝕣𝕠𝕤𝕠𝕗𝕥 changed their way to redeem _(even the latest version is V1.5)_ and successfully redeemed rewards _(For those who want to know, I'm not flexing 🥴)_
</Details>

<Details>
<summary>

###### 7. Can I setup more than 20 bot jobs?

</summary>

- Maybe you can. Go to [Python code to help creating YML](Python code to help creating YML), there are some codes that help you implement that. But you might try yourself, because it's not only clone the commands. You have to change few stuff in distribute step and... ye :v
</Details>

<!-- Foot hyperlinks: -->

[Changelog]: Changelogs.md
[𝕗𝕒𝕣𝕤𝕙𝕒𝕕𝕫𝟙𝟡𝟡𝟟's bot repo]: https://github.com/farshadz1997/Microsoft-Rewards-bot
[𝕗𝕒𝕣𝕤𝕙𝕒𝕕𝕫𝟙𝟡𝟡𝟟's discord group]: https://discord.gg/GaF8fFBtE3
[𝕗𝕒𝕣𝕤𝕙𝕒𝕕𝕫𝟙𝟡𝟡𝟟's Telegram group]: https://telegram.me/MRF_FZ
[Telegram]: https://t.me/KevinNitro
[MSRB Original]: YML%20files/MSRB%20Original.yml
[MSRB Multiple]: YML%20files/MSRB%20Multiple.yml
[MSRB Multiple Advanced]: YML%20files/MSRB%20Multiple%20Advanced.yml
[Github Action limits]: https://gitlab.com/KevinNitro/msrb-ga#%F0%9D%94%BE%F0%9D%95%80%F0%9D%95%8B%E2%84%8D%F0%9D%95%8C%F0%9D%94%B9-%F0%9D%94%B8%E2%84%82%F0%9D%95%8B%F0%9D%95%80%F0%9D%95%86%E2%84%95-%F0%9D%95%80%E2%84%95%F0%9D%94%BD%F0%9D%95%86%E2%84%9D%F0%9D%95%84%F0%9D%94%B8%F0%9D%95%8B%F0%9D%95%80%F0%9D%95%86%E2%84%95
[Create Github Action Secret]: https://gitlab.com/KevinNitro/msrb-ga/-/tree/main#i-how-to-create-a-%F0%9D%94%BE%F0%9D%95%80%F0%9D%95%8B%E2%84%8D%F0%9D%95%8C%F0%9D%94%B9-%F0%9D%94%B8%E2%84%82%F0%9D%95%8B%F0%9D%95%80%F0%9D%95%86%E2%84%95-%F0%9D%95%8A%F0%9D%94%BC%E2%84%82%E2%84%9D%F0%9D%94%BC%F0%9D%95%8B
[Create secret Gist]: https://gitlab.com/KevinNitro/msrb-ga/-/blob/main/README.md#ii-how-to-create-secret-gist-for-accountsjson-raw-link
[𝕗𝕒𝕣𝕤𝕙𝕒𝕕𝕫𝟙𝟡𝟡𝟟's bot commit]: https://github.com/farshadz1997/Microsoft-Rewards-bot/commits/master
