var = r'''  Run-Bot-1:
    runs-on: ubuntu-latest
    needs: Setup-Bot
    if: ${{ needs.Setup-Bot.outputs.NUMBER_OF_JOBS >= 1 }}
    steps:

    - name: Set Current-Bot Value
      run: echo "Current-Bot=1" >> $GITHUB_ENV

    - name: Download artifact MSRB
      uses: actions/download-artifact@main
      with:
        name: MSRB_Artifact

    - name: Cache Session
      if: ${{ needs.Setup-Bot.outputs.HAS_SESSION == 'True' }}
      id: cache_session
      uses: actions/cache@main
      with:
        key: Cache-${{ env.Current-Bot }}-${{ hashFiles('MSRB/1/accounts_for_session_cache.txt') }}
        restore-keys: Cache-${{ env.Current-Bot }}
        path: |
          **/Profiles

    - name: Check Public IP
      if: ${{ env.CHECK_IP == 'True' }}
      uses: haythem/public-ip@master

    - name: Set up Python 3.10
      uses: actions/setup-python@v4
      with:
        python-version: "3.10"
        cache: 'pip'

    - name: Clone Bot into running folder
      run: cp Microsoft-Rewards-bot/* MSRB/${{ env.Current-Bot }}

    - name: Install dependencies
      run: pip install -r MSRB/${{ env.Current-Bot }}/requirements.txt

    - name: Run the Bot
      run: |
        cd MSRB/${{ env.Current-Bot }}
        ${{ needs.Setup-Bot.outputs.COMMAND }}

    - name: Retry the Bot
      if: ${{ failure() && env.RETRY_THE_BOT == 'True' }}
      run: |
        cd MSRB/${{ env.Current-Bot }}
        rm Logs_accounts.txt
        ${{ needs.Setup-Bot.outputs.RETRY_COMMAND }}

    - name: Print out the log after run
      if: ${{ env.PRINT_OUT_THE_LOG_AFTER_RUNNING_BOT == 'True' }}
      run: cat MSRB/${{ env.Current-Bot }}/Logs_accounts.txt

    - name: Setup session cache
      if: steps.cache_session.outputs.cache-hit != 'true' && ${{ needs.Setup-Bot.outputs.HAS_SESSION == 'True' }}
      run: |
        Profiles_path="MSRB/${{ env.Current-Bot }}/Profiles"
        accounts_for_session_cache_path="MSRB/${{ env.Current-Bot }}/accounts_for_session_cache.txt"

        find "$Profiles_path" -type d -name "Cache" -o -name "Code Cache" | while read -r folder; do
          echo "Deleting folder: $folder"
          rm -rf "$folder"
        done

        mapfile -t usernames < "$accounts_for_session_cache_path"

        for folder in "$Profiles_path"/*; do
          folder_name=$(basename "$folder")
          
          if [[ ! " ${usernames[@]} " =~ " $folder_name " ]]; then
            echo "Deleting folder: $folder"
            rm -rf "$folder"
          fi
        done
'''

for i in range(2,21):
	modify = var.replace('Run-Bot-1','Run-Bot-' + str(i)).replace('env.Bot_1','env.Bot_' + str(i)).replace('echo "Current-Bot=1','echo "Current-Bot=' + str(i)).replace('>= 1','>= '+str(i)).replace("MSRB/1/accounts_for_session_cache.txt","MSRB/"+str(i)+"/accounts_for_session_cache.txt")
	print(modify)