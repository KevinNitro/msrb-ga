# CHANGELOGS

## [V1.6](https://gitlab.com/KevinNitro/msrb-ga/-/tree/V1.6)

- Reconstruct `MSRB Multiple.yml`
- Support `--session` for all scripts _(less than about 75 accounts in accounts.json)_
- Resync construction of 3 scripts
- Add `CHECK_MAX_NUMBER_OF_SPLIT_ACCOUNTS` feature

## [V1.5](https://gitlab.com/KevinNitro/msrb-ga/-/tree/V1.5)

- Introduce `MSRB Multiple Parallel.yml`
- Remove **Commit all repo after run** feature
- Add author, Notes inside of yml files
- Change codes
- Move variable `GITHUB_TOKEN` to Delete workflows step
- Add `CHECK_IP`, `DELETE_ARTIFACT`, variables for `MA`
- Add how to run on [README.md](README.md)

## [V1.4](https://gitlab.com/KevinNitro/msrb-ga/-/tree/V1.4)

- Now you can turn features `on`/`off` easier by editing **variables' value**
- Change the time for the old workflows to be deleted to `1` second
- Change the name of workflows to other names _(Deploy)_

- MSRB:
  - Add step **Check IP**

- MSRB Multiple:
  - Fix error at **Check log after run** step
  - Change some codes

## [V1.3](https://gitlab.com/KevinNitro/msrb-ga/-/tree/V1.3)

- MSRB:
  - Now as **Original version** _(run sequently in 1 job)_
  - Change the code, replace `DEFAULT_COMMAND`, `DEFAULT_RETRY_COMMAND`, `CUSTOM_COMMAND`, `CUSTOM_RETRY_COMMAND` with `COMMAND` & `RETRY_COMMAND`
  - Auto add `--on-finish exit` into `COMMAND` & `RETRY_COMMAND`, no need to add manually
  - Support turn features on/off easier

- MSRB Multiple:
  - Introduce MSRB Multiple

## [V1.2](https://gitlab.com/KevinNitro/msrb-ga/-/tree/V1.2)

- Add version in code
- Support run without `--session` _(But need `--session` in **DEFAULT_COMMAND** and **DEFAULT_RETRY_COMMAND** )_
- Hide log when fetch accounts.json from raw link

## [V1.1](https://gist.github.com/KevinNitroG/5b02b468f4f76d4d82941f7b41d8af1d/918827ca316f7e8ce798e14ac65c3df901123a04#file-msrb-yml)

- Add `SPECIFIC_VERSION`: Which can use the bot at specific version
- Change code _(Merge Run the Bot and Retry the Bot)_
- Change `DEFAULT_COMMAND` and `DEFAULT_RETRY_COMMAND`: use `--headless` instead of `--virtual-display` and remove `--shuffle`

## [V1.0](https://gist.github.com/KevinNitroG/5b02b468f4f76d4d82941f7b41d8af1d/aa43aa5ee4d77121c08856064450afe84cd0930a#file-msrb-yml)

- idk what to write
